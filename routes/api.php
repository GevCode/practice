<?php

use Illuminate\Http\Request;
//use Illuminate\Routing\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', 'UserController@index');
Route::get('/user/{user}', 'UserController@show');
Route::get('/projects/{user}', 'ProjectController@index');
Route::get('/project/{project}', 'ProjectController@show');
Route::post('/user/{user}/projects/create', 'ProjectController@store');
Route::get('/categories', 'ProjectController@categories');



<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class UserController extends Controller {

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request) {
        $search = $request->query('search');
        $query = User::query();

        if($search) {
            $query->when(request('searchBy') == 'name', function ($q) {
                return $q->where('name', 'like', request('search').'%');
            });
            $query->when(request('searchBy') == 'project', function ($q) {
                return $q->whereHas('projects', function ($qu) {
                    return $qu->where('title', 'like', request('search').'%');
                });
            });
        }
        $users = $query->with('projects')->paginate(10);

        return Response::json([
            'success' => true,
            'users' => $users
        ]);
    }

    /**
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(User $user) {
        return Response::json([
            'success' => true,
            'user' => $user
        ]);
    }
}

<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use App\Project;
use App\User;

class ProjectController extends Controller {

    /**
     * @param User $user
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(User $user, Request $request) {
        $orderBy = $request->query('orderBy', 'id');
        $search = $request->query('search');

        $projects = Project::with(['tasks', 'categories'])
            ->where('user_id', $user->id);
        if($search) {
            $projects = $projects
                ->where('title', 'LIKE', $search . '%');
        }
        if($request->has('tag')) {
            $projects = Category::findOrFail($request->tag)
                ->projects()->with('categories')
                ->where('user_id', '=', $user->id);
        }
        $projects = $projects->orderBy($orderBy)->get();
        return Response::json([
            'success' => true,
            'projects' => $projects
        ]);
    }

    /**
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, User $user) {
        $tags = $request->tags;
        $user->makeProject($request->validate([
            'title' => ['required', 'min:3'],
            'description' => ['required', 'min:7']
        ]))
            ->categories()
            ->attach(
                array_map(function ($tag) {
                    return Category::firstOrCreate(['category' => $tag])->id;
                }, $tags)
            );

        return Response::json(['success' => true]);
    }

    public function categories(Request $request) {
        $tags = Category::query();
        if($request->has('all')) {
            $tags = $tags->withCount('projects')->get();
        } elseif($request->has('tagSearch')) {
            $tags = $tags
                ->where('category', 'like', $request->tagSearch . '%')->get();
        } else {
            return Response::json(['success' => false]);
        }
        return Response::json([
            'success' => true,
            'tags' => $tags
        ]);
    }

    /**
     * @param Project $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Project $project) {
        return Response::json([
            'success' => true,
            'project' => $project->load(['tasks'])
        ]);
    }

    public function edit(Project $project) {

        return view('projects.edit', compact('project'));

    }

    public function update(Project $project) {

        $project->update(request()->all());
        return redirect('/projects');

    }

    public function destroy($id) {

        Project::findOrFail($id)->delete();

        return redirect('/projects');

    }

}

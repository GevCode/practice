@extends('layout')

@section('content')

    <h1>Create ProjectShow</h1>

    <form method="post" action="/projects">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" name="title" id="title" class="form-control {{$errors->has('title') ? 'border border-danger' : ''}}" value="{{old('title')}}">
            @if ($errors->has('title'))
                @foreach($errors->get('title') as $msg)
                    <p class="small text-danger">{{$msg}}</p>
                @endforeach
            @endif
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <textarea name="description" id="description" class="form-control {{$errors->has('description') ? 'border border-danger' : ''}}" rows="3">{{old('description')}}</textarea>
            @if ($errors->has('description'))
                @foreach($errors->get('description') as $msg)
                    <p class="small text-danger">{{$msg}}</p>
                @endforeach
            @endif
        </div>
        <button type="submit" class="btn btn-info">Create ProjectShow</button>
    </form>

@endsection


@extends('layout')

@section('sidebar')
    <a class="btn btn-outline-info" href="/user/{{ $project->user_id }}">
        <i class="fas fa-long-arrow-alt-left mr-2"></i>
        All Projects
    </a>
@endsection

@section('content')

    <h1>{{$project->title}}</h1>
    <p>{{$project->description}}</p>

    <div class="row align-items-center py-3">
        <div class="col">
            <h3 class="mb-0">Tasks</h3>
        </div>
        <div class="col-auto">
            {{--add new task form--}}
            <form class="form-inline" method="post" action="/projects/{{$project->id}}/tasks">
                {{csrf_field()}}
                <div class="form-group">
                    <input class="form-control rounded-0" type="text" placeholder="Add a new task" name="description">
                    <button class="btn btn-outline-info rounded-0" type="submit">Add</button>
                </div>
            </form>
        </div>
    </div>


    {{--tasks table--}}
    @if ($project->tasks->count())
    <table class="table table-striped border mt-3">
        @foreach($project->tasks as $key => $task)
            <tr>
                <td>{{ $key+1 }}</td>
                <td class="{{ $task->completed ? 'is-complete' : '' }}">{{ $task->description }}</td>
                <td>
                    <form method="post" action="/tasks/{{ $task->id }}">
                        {{csrf_field()}}
                        {{method_field('patch')}}
                        <div class="form-group">
                            <input id='completed' type="checkbox" name="completed" onchange="this.form.submit()" {{ $task->completed ? 'checked' : '' }}>
                        </div>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
    @else
    <p class="text-secondary">No tasks yet</p>
    @endif

@endsection
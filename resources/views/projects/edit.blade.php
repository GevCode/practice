@extends('layout')

@section('content')

    <h1>Edit project</h1>

    <form method="post" action="/projects/{{$project->id}}">
        {{csrf_field()}}
        {{ method_field('patch') }}
        <div class="form-group">
            <label for="title">Title</label>
            <input name="title" type="text" class="form-control" id="title" value="{{$project->title}}">
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <textarea name="description" class="form-control" id="description" rows="3">{{$project->description}}</textarea>
        </div>
        <button type="submit" class="btn btn-info">Update ProjectShow</button>
    </form>



@endsection
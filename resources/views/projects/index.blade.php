@extends('layout')

@section('content')
    <div class="row">
        <div class="col">
            <h1>Projects</h1>
        </div>
        <div class="col-auto">
            <a class="btn btn-outline-info" href="/projects/create">
                <i class="fas fa-plus"></i>
            </a>
        </div>
    </div>
    <table class="table table table-striped mt-3">
        @foreach($projects as $key => $project)
            <tr>
                <th scope="row">{{$key+1}}</th>
                <td>
                    <a class="text-dark" href="/projects/{{$project->id}}">{{$project->title}}</a>
                </td>
                <td>
                    <div class="row justify-content-end">
                        <div class="col-auto">
                            <a class="btn btn-outline-info" href="/projects/{{$project->id}}/edit">
                                <i class="far fa-edit"></i>
                            </a>
                        </div>
                        <div class="col-auto">
                            <form class="form-inline" method="post" action="/projects/{{$project->id}}">
                                {{method_field('DELETE')}}
                                {{csrf_field()}}

                                <button class="btn btn-outline-info border-0'" type="submit">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </form>
                        </div>
                    </div>
                </td>
            </tr>
        @endforeach
    </table>
@endsection

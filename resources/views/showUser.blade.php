@extends('layout')

@section('sidebar')
    <a class="btn btn-outline-info" href="/">
        <i class="fas fa-long-arrow-alt-left mr-2"></i>
        All Users
    </a>
@endsection

@section('content')

    <h1>{{$user->name}}</h1>

    <div class="row pt-3">
        <div class="col">
            <h3>Projects</h3>
        </div>
        <div class="col-auto">
            <a class="btn btn-outline-info" href="/projects/create">
                <i class="fas fa-plus"></i>
            </a>
        </div>
    </div>
    @if($user->projects->count())
        <table class="table table-striped mt-3">
            @foreach($user->projects as $key => $project)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>
                        <a class="text-dark" href="/projects/{{ $project->id }}">{{$project->title}}</a>
                    </td>
                    <td>Tasks {{$project->tasks->count()}}</td>
                </tr>
            @endforeach
        </table>
    @else
        <p class="text-secondary">No projects yet</p>
    @endif

@endsection

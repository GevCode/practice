import React, { Component } from 'react';
import axios from 'axios';

class NewProject extends Component{
    constructor(props) {
        super(props);
        this.state = {
            tagSearch: [],
            tags: [],
            tagsCount: 0,
            tagsLimit: 3,
            activeTag: -1,
            title: '',
            description: '',
            errors: []
        };

        this.handleFieldChange = this.handleFieldChange.bind(this);
        this.handleCreateNewProject = this.handleCreateNewProject.bind(this);
        this.hasErrorFor = this.hasErrorFor.bind(this);
        this.renderErrorFor = this.renderErrorFor.bind(this);
        this.handleAddTag = this.handleAddTag.bind(this);
        this.handleRemoveTag = this.handleRemoveTag.bind(this);
        this.handleChooseTag = this.handleChooseTag.bind(this);
    }

    handleAddTag (newTag) {
        event.preventDefault();
        const tagArr = this.state.tags;
        if(tagArr.find((tag) => tag === newTag)) { //checks if the new tag already exists and adds message
            this.setState({
                tagSearch: [{ alreadyExists: "You've added this tag"}]
            });
            setTimeout(() => {
                this.setState({
                    tagSearch: []
                });
            }, 3000)
        } else { //add new tag to the tags array
            tagArr.push(newTag);
            this.setState({
                tags: tagArr,
                tagSearch: [],
                tagsCount: this.state.tagsCount + 1
            });
        }

    }

    handleRemoveTag (tag,event) {
        event.preventDefault();
        const tagArr = this.state.tags;
        tagArr.splice(tagArr.indexOf(tag), 1);
        this.setState({
            tags: tagArr,
            tagsCount: this.state.tagsCount - 1
        })
    }

    handleChooseTag(event) {
        const tagSearch = this.state.tagSearch;
        if(tagSearch) {
            const activeTag = this.state.activeTag;
            if(event.keyCode === 40) {
                if(activeTag < tagSearch.length - 1) {
                    this.setState({
                        activeTag: this.state.activeTag + 1
                    })
                } else {
                    this.setState({
                        activeTag: 0
                    })
                }
            } else if(event.keyCode === 38) {
                if(activeTag > 0) {
                    this.setState({
                        activeTag: this.state.activeTag -1
                    })
                } else {
                    this.setState({
                        activeTag: tagSearch.length - 1
                    })
                }
            }
            if(event.keyCode === 27) {
                this.setState({
                    tagSearch: [],
                    activeTag: -1
                })
            }
            if(event.keyCode === 13 && this.state.activeTag >= 0) {
                event.preventDefault();
                const newTag = this.state.tagSearch[this.state.activeTag].category;
                this.handleAddTag(newTag);
                this.setState({
                    activeTag: -1
                })
            }
        }
    }

    handleFieldChange (event) {
        const value = event.target.value;
        const name = event.target.name;

        if(name === 'tags') {
            const params = {};
            if(this.timeout) {
                clearTimeout(this.timeout);
            }
            if(value.length > 1) {
                params.tagSearch = value;
                this.setState({
                    tagSearch: [{category: value}]
                });
            } else {
                this.setState({
                    tagSearch: []
                })
            }
            this.timeout = setTimeout(() => {
                axios.get('/api/categories', {params})
                    .then(response => {
                        if(response.data.success) {
                            this.setState({
                                tagSearch: this.state.tagSearch.concat(response.data.tags)/*[{category: value}, ...response.data.tags]*/
                            })
                        }
                        /*else {
                            this.setState({
                                tagSearch: [{category: value}]
                            })
                        }*/
                    })
            }, 300);

        } else {
            this.setState({
                [name] : value
            })
        }
    }

    handleCreateNewProject (event) {
        event.preventDefault();

        const userId = this.props.match.params.id;
        const project = {
            tags: this.state.tags,
            title: this.state.title,
            description: this.state.description
        };
        axios.post(`/api/user/${userId}/projects/create`, project)
            .then(response => {
                if(response.data.success) {
                    this.props.history.push(`/user/${userId}`)
                }

            })
            .catch(error => {
                this.setState({
                    errors: error.response.data.errors
                })
            })
    }

    hasErrorFor (field) {
        return !!this.state.errors[field]
    }

    renderErrorFor (field) {
        if(this.hasErrorFor(field)) {
            return (
                this.state.errors[field].map((error) => {
                    return (
                        <div key={error} className='invalid-feedback'>
                            <span>{error}</span>
                        </div>
                    )
                })

            )
        }
    }

    renderSearchResult() {
        const tagSearch = this.state.tagSearch;
        const activeTag = this.state.activeTag;
        if(tagSearch.length > 0) {
            if(tagSearch[0].alreadyExists) {
                return (
                    <div data-spy="scroll" className="scroll searchDiv p-2 position-absolute border rounded-bottom bg-white">
                        <div>
                            <span className="small">
                                {tagSearch[0].alreadyExists}
                            </span>
                        </div>
                    </div>
                )
            }
            return(
                <div data-spy="scroll" className="scroll searchDiv position-absolute border rounded-bottom bg-white py-2">
                    {tagSearch.map((tag,i) => {
                        return(
                            <div
                                className={(activeTag === i) ?
                                    "px-2 w-100 text-info searchResults activeTag" :
                                    "px-2 w-100 text-info searchResults"}
                                key={i} onClick={() => this.handleAddTag(tag.category)}
                            >
                                {tag.category}
                            </div>
                        )
                    })}
                </div>
            )
        }
    }

    renderTags () {
        if(this.state.tags.length > 0) {
            return (
                this.state.tags.map((tag,i) => {
                    return(
                        <div key={i} className="col-auto mr-2 d-flex align-items-center border border-info rounded bg-light text-info">
                            <span className="mr-2 font-weight-bold">{tag}</span>
                            <a href='#' className="text-info" onClick={(e) => this.handleRemoveTag(tag,e)}><i className="fas fa-times"></i></a>
                        </div>
                    )
                })
            )
        }
    }

    renderSearchBar() {
        if(this.state.tagsCount < this.state.tagsLimit) {
            return (
                <input
                    id="tags"
                    type="text"
                    name="tags"
                    className="form-control border-0 p-0"
                    onChange={this.handleFieldChange}
                    onKeyDown={this.handleChooseTag}
                    placeholder="Search or add tag..."
                />
            )
        }
    }

    render() {

        return (
            <div className="container">
                <div className="row justify-content-center p-3">
                    <div className="col-8 border bg-gray rounded shadow-sm p-3">
                        <h1>New Project</h1>
                        <form autoComplete="off" onSubmit={() => { return false}}>
                            <div className="form-group">
                                <label htmlFor="tags">Add tags (up to 3)</label>
                                <div className="row p-2 bg-white input-border mx-0 rounded">
                                    {this.renderTags()}
                                    <div className="col-3 px-0 position-relative">
                                        {this.renderSearchBar()}
                                        {this.renderSearchResult()}
                                    </div>
                                </div>
                            </div>
                            <div className="form-group">
                                <label htmlFor="title">Title</label>
                                <input
                                    id="title"
                                    type="text"
                                    className={`form-control ${this.hasErrorFor('title') ? 'is-invalid' : ''}`}
                                    name="title"
                                    value={this.state.title}
                                    onChange={this.handleFieldChange}
                                />
                                {this.renderErrorFor('title')}
                            </div>
                            <div className="form-group">
                                <label htmlFor="description">Description</label>
                                <textarea
                                    id="description"
                                    className={`form-control ${this.hasErrorFor('description') ? 'is-invalid' : ''}`}
                                    name="description"
                                    rows="5"
                                    value={this.state.description}
                                    onChange={this.handleFieldChange}
                                />
                                {this.renderErrorFor('description')}
                            </div>
                            <button
                                type="button"
                                className="btn btn-info"
                                onClick={this.handleCreateNewProject}
                            >
                                Create Project
                            </button>
                        </form>
                    </div>
                </div>

            </div>
        )
    }

}

export default NewProject
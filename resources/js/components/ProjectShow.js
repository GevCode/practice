import React, { Component } from 'react';
import axios from 'axios';
import {Link} from "react-router-dom";
import Sidebar from "./Sidebar";

class ProjectShow extends Component {
    constructor(props) {
        super(props);
        this.state = {
            project: {},
            tasks: [],
        };
    }

    componentWillMount() {
        const projectId = this.props.match.params.id;
        axios.get(`/api/project/${projectId}`)
            .then(response => {
                if(response.data.success) {
                    this.setState({
                        project: response.data.project,
                        tasks: response.data.project.tasks
                    })
                }
            })
    }

    render() {
        const project  = this.state.project;
        const tasks = this.state.tasks;

        const tasksList = (tasks.length) ?
            (<table className="table table-striped mt-3">
                <tbody>
                {tasks.map((task, key) => {
                    return(
                        <tr key={key}>
                            <td>{key+1}</td>
                            <td>
                                <span className="text-dark">{task.description}</span>
                            </td>
                        </tr>
                    )
                })}
                </tbody>
            </table>) : (<p className="text-secondary">No tasks yet</p>);

        return(
            <div className="container border shadow">
                <div className="row py-3">
                    <Sidebar href={`/user/${project.user_id}`} goto='Projects'/>
                    <div className="col-md-10">
                        <h2>{ project.title }</h2>
                        <p>{ project.description }</p>
                        <div className="row pt-3">
                            <div className="col">
                                <h3>Tasks</h3>
                            </div>
                            <div className="col-auto">
                                <Link to={"/projects/create"}>
                                        <span className="btn btn-outline-info">
                                            <i className="fas fa-plus"></i>
                                        </span>
                                </Link>
                            </div>
                        </div>
                        { tasksList }
                    </div>
                </div>
            </div>
        )
    }

}

export default ProjectShow;
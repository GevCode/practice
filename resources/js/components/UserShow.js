import React, { Component } from 'react';
import axios from 'axios';
import {Link} from "react-router-dom";
import Sidebar from "./Sidebar";
import ProjectItemDiv from "./ProjectItemDiv";
import ProjectItemTr from "./ProjectItemTr";

export default class UserShow extends Component{
    constructor (props) {
        super(props);
        this.state = {
            filters: {

            },
            orderBy: 'id',
            search: null,
            filter: null,
            showAsTr: true,
            user: {},
            projects: [],
            tags: [],
            tagsCount: null
        };

        this.handleSwitch = this.handleSwitch.bind(this);
        this.handleOrderBy = this.handleOrderBy.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
        this.handleSwitch = this.handleSwitch.bind(this);
        this.handleFilter = this.handleFilter.bind(this);

    }

    handleSwitch(param) {
        this.setState({
            showAsTr: param
        })
    }

    handleOrderBy(event) {
        const userId = this.props.match.params.id;
        this.setState({
            orderBy: event.target.value
        },() => {
            const params = {
              orderBy: this.state.orderBy,
              search: this.state.search,
              filter: this.state.filter
            };
            this.getUserProjects(userId, params)
        })
    }

    handleSearch(event) {
        if(this.timeOut) {
            clearTimeout(this.timeOut);
        }
        this.setState({
            search: event.target.value
        }, () => {
            this.timeOut = setTimeout(() => {
                const userId = this.props.match.params.id;
                const params = {
                    orderBy: this.state.orderBy,
                    search: this.state.search,
                    filter: this.state.filter
                };
                this.getUserProjects(userId, params)
            }, 700);
        })
    }

    handleFilter(tagId) {
        const userId = this.props.match.params.id;
        const params = {
            tag: tagId
        };
        this.getUserProjects(userId, params);
    }

    getUserProjects(userId, params) {
        axios.all([
            axios.get(`/api/user/${userId}`),
            axios.get(`/api/projects/${userId}`,{ params })
        ]).then(axios.spread((userRes, projectsRes) => {
            if(userRes.data.success && projectsRes.data.success) {
                this.setState({
                    user: userRes.data.user,
                    projects: projectsRes.data.projects,
                })
            }

        }))
    }

    componentWillMount() {
        const userId = this.props.match.params.id;
        const params = {
            orderBy: this.state.orderBy,
            search: this.state.search,
            filter: this.state.filter
        };
        this.getUserProjects(userId, params);
        this.getTags();
    }

    renderItem() {
        if(this.state.projects.length){
            if(this.state.showAsTr){
                return (<table className="table table-striped mt-3">
                    <tbody>
                        {this.state.projects.map((project, key) => {
                            return <ProjectItemTr model={project} key={key}/>
                        })}
                    </tbody>
                </table>);
            }else{
                return (
                    <div className="row mx-0 py-3">
                        {this.state.projects.map((project, key) => {
                            return <ProjectItemDiv model={project} key={key}/>
                        })}
                    </div>
                )
            }
        }else{
            return <div className="m-1 text-secondary">'No projects yet'</div>
        }
    }

    getTags() {
        axios.get('/api/categories', {
            params: {
                all: true
            }
        }).then(response => {
            if(response.data.success) {
                console.log(response.data);
                const tags = response.data.tags;
                const usages = tags.map(tag => {
                    return tag.projects_count
                });
                const minUsed = Math.min(...usages);
                const maxUsed = Math.max(...usages);
                const coefficient = (tags.length/(maxUsed - minUsed));
                console.log(minUsed,maxUsed, coefficient*minUsed);
                this.setState({
                    tags: tags,
                    tagsCount: tags.length,
                    minUsed: minUsed,
                    maxUsed: maxUsed,
                    coeff: coefficient
                })
            }
        })
    }

    getTagSize(currentTagUsage) {

    }

    renderTags() {
        return this.state.tags.map((tag, i) => {
           return(
               <button
                   key={i}
                   className="btn btn-outline-info m-1"
                   onClick={() => this.handleFilter(tag.id)}
               >
                   {tag.category}
               </button>
           )
        });
    }

    render() {
        const user  = this.state.user;

        return(
            <div className="container">
                <div className="row py-3">
                    <div className="col-md-3 border rounded bg-white py-3">
                        <div className="row">
                            <div className="col-12">
                                <Link to={'/'}>
                                    <span className="btn btn-outline-info">
                                        <i className="fas fa-long-arrow-alt-left mr-2"></i>
                                        All Users
                                    </span>
                                </Link>
                            </div>
                            <div className="col-12 py-3">
                                <h4>Filters</h4>
                                <ul className="list-group">
                                    <li className="list-group-item border-0 p-0">
                                        <input id="no-tasks" type="checkbox" onClick={this.handleFilter}/>
                                        <label htmlFor="no-tasks" className="ml-2">Projects with no tasks</label>
                                    </li>
                                </ul>
                            </div>
                            <div className="col">
                                {this.renderTags()}
                            </div>
                        </div>
                    </div>
                    <div className="col-md-9">
                        <div className="row ml-0 border rounded bg-white mb-3 py-3 shadow-sm">
                            <div className="col ">
                                <h2 className="mb-0">{ user.name }</h2>
                            </div>
                            <div className="col-auto">
                                <form className="form-inline">
                                    <div className="form-group">
                                        <label htmlFor="search" className="sr-only">Search Projects</label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="search"
                                            placeholder="Search Projects"
                                            onChange={this.handleSearch}
                                        />
                                    </div>
                                    <button type="submit" className="btn btn-info ml-2">Go</button>
                                </form>
                            </div>
                        </div>
                        <div className="row border rounded bg-white ml-0 py-3 shadow-sm">
                            <div className="col">
                                <h3 className="mb-0">Projects</h3>
                            </div>
                            <div className="col">
                                <div className="btn-group" role="group" aria-label="Basic example">
                                    <button type="button" className="btn btn-outline-info" onClick={()=>{
                                        this.handleSwitch(true);
                                    }}>
                                        <i className="fas fa-list-ul"></i>
                                    </button>
                                    <button type="button" className="btn btn-outline-info" onClick={()=>{
                                        this.handleSwitch(false);
                                    }}>
                                        <i className="fas fa-th"></i>
                                    </button>
                                </div>
                            </div>
                            <div className="col">
                                <select className="form-control" onChange={this.handleOrderBy}>
                                    <option value="id">Order By</option>
                                    <option value="title">Title</option>
                                    <option value="description">Content</option>
                                    <option value="created_at">Last Created</option>
                                </select>
                            </div>
                            <div className="col-auto">
                                <Link to={`/user/${ user.id }/projects/create`}>
                                    <span className="btn btn-outline-info">
                                        Add New Project
                                    </span>
                                </Link>
                            </div>
                            <div className="col-12">
                                { this.renderItem() }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
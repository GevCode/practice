import React, { Component } from 'react';
import { Link } from "react-router-dom";

export default class Sidebar extends Component{
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <div className="col-md-3 border rounded bg-white shadow-sm py-3 justify-content-center">
                <Link to={this.props.href}>
                    <span className="btn btn-outline-info">
                        <i className="fas fa-long-arrow-alt-left mr-2"></i>
                        All {this.props.goto}
                    </span>
                </Link>
            </div>
        )
    }
}
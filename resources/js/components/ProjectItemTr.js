import React, { Component } from 'react';
import {Link} from "react-router-dom";

export default class ProjectItemTr extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const project = this.props.model;
        return(
            <tr>
                <td>{ project.id }</td>
                <td>
                    <Link to={`/project/${project.id}`}>
                        <span className="text-dark">{ project.title }</span>
                    </Link>
                    <div>
                        {project.categories.map(
                            (item, key) => <a key={key} href="#" className="small text-info mr-1">
                                #{item.category}
                            </a>
                        )}
                    </div>
                </td>
                <td>Tasks: {project.tasks.length}</td>
            </tr>
        )
    }
}
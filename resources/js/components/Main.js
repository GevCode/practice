import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import UserList from "./UserList";
import UserShow from './UserShow';
import ProjectShow from './ProjectShow';
import NewProject from "./NewProject";


class Main extends Component{
    render () {
        return(
            <div>
                <BrowserRouter>
                    <Switch>
                        <Route exact path='/' component={UserList}/>
                        <Route exact path='/user/:id' component={UserShow} />
                        <Route exact path='/project/:id' component={ProjectShow} />
                        <Route exact path='/user/:id/projects/create' component={NewProject} />
                    </Switch>
                </BrowserRouter>
            </div>

        );
    }
}

const root = document.getElementById('root');
if(root) {
    ReactDOM.render(< Main />, root);
}
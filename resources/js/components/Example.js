/*import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export default class Example extends Component {
    render() {
        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-8">
                        <div className="card">
                            <div className="card-header">Example Component</div>

                            <div className="card-body">
                                I'm an example component!
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

if (document.getElementById('example')) {
    ReactDOM.render(<Example />, document.getElementById('example'));
}*/

/*function Animal(name, voice) {
   this.name = name;
   this.voice = voice;
}

Animal.prototype.say = () => {
    console.log(this.name, 'goes', this.voice)
};*/

class Animal {
    constructor(name, voice) {
        this.name = name;
        this.voice = voice;
    }

    say () {
        console.log(this.name, 'goes', this.voice);
    }
}

class Bird extends Animal {
    constructor(name, voice, canFly) {
        super(name, voice);
        this.canFly = canFly;
        this.fly();
    }

    fly () {
        if(this.canFly) {
            console.log(this.name, 'is flying');
        } else {
            console.log(this.name, 'can not fly');
        }
    }
}


class Counter {
    constructor() {
        this.count = 0;
        this.increment = () => {
            this.count += Counter.incStep
        };
    }
}

Counter.incStep = 2;
Counter.incrementAll = (arr) => {
  arr.forEach((item) => item.increment());
};

Counter.incrementAll([1,3,5]);

// const dog = new Animal('dog', 'woof');
// const cat = new Animal('cat', 'meow');
// const duck = new Bird('duck', 'quack', true);
// const penguin = new Bird('penguin', 'quack', false);

// dog.say();
// cat.say();
// duck.say();


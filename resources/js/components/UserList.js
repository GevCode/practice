import React, { Component } from 'react';
import axios from 'axios';
import {Link} from "react-router-dom";
import Sidebar from "./Sidebar";

class UserList extends Component{

    constructor(props) {
        super(props);
        this.state = {
            searchUser: null,
            searchBy: 'name',
            pagesCount: null,
            currentPage: 1,
            users: [],
        };

        this.handleSearchUser = this.handleSearchUser.bind(this);
        this.handlePaginate = this.handlePaginate.bind(this);
    }

    getUsers(params) {
        axios.get('/api/', { params })
            .then((response) => {
                if(response.data.success) {
                    this.setState({
                        users: response.data.users.data,
                        currentPage: response.data.users.current_page,
                        pagesCount: response.data.users.last_page
                    })
                }
            })
    }

    handleSearchUser(event) {
        if(this.timeOut) {
            clearTimeout(this.timeOut);
        }
        this.setState({
            searchUser: event.target.value
        }, () => {
            this.timeOut = setTimeout(()=> {
                const params = {
                    search: this.state.searchUser,
                    searchBy: this.state.searchBy
                };
                this.getUsers(params)
            }, 700)
        });

    }

    handleSearchBy(event) {
        this.setState({
            searchBy: event.target.value
        })
    }

    handlePaginate(page) {
        const currentPage = this.state.currentPage;
        const pagesCount = this.state.pagesCount;
        if(page === 'next') {

            page = (currentPage < pagesCount) ? currentPage + 1 : 1;
        } else if( page === 'prev') {
            page = (currentPage > 1) ? currentPage - 1 : pagesCount;
        }

        const params = { page };
        this.getUsers(params);
    }

    componentWillMount() {
        this.getUsers()
    }

    renderUsers() {
        return this.state.users.map((user)=> {
            return (
                <tr key={user.id }>
                    <td>{ user.id }</td>
                    <td>
                        <Link to={`/user/${user.id}`}>
                            <span className="text-dark">{ user.name }</span>
                        </Link>
                    </td>
                    <td>Projects: { user.projects.length }</td>
                </tr>
            );
        })
    }

    paginate() {
        const currentPage = this.state.currentPage;

        const paginations = [
            <li key="prev" className="page-item">
                <a className="page-link" aria-label="Previous" href="#" onClick={() => this.handlePaginate('prev')}>
                    <span aria-hidden="true">&laquo;</span>
                    <span className="sr-only">Previous</span>
                </a>
            </li>
        ];

        for(let page = 1; page <= this.state.pagesCount; page++) {
            paginations.push(
                <li key={page} className="page-item">
                    <a
                        onClick={() => this.handlePaginate(page)}
                        href="#"
                        className={(currentPage === page) ? "page-link activePage" : "page-link"}
                    >
                        {page}
                    </a>
                </li>
            )
        }

        paginations.push(
            <li key="next" className="page-item">
                <a className="page-link" aria-label="Next" href="#" onClick={() => this.handlePaginate('next')}>
                    <span aria-hidden="true">&raquo;</span>
                    <span className="sr-only">Next</span>
                </a>
            </li>
        );

        return paginations;
    }

    render () {
        return(
            <div className="container">
                <div className="row">
                    <Sidebar href='/'/>
                    <div className="col-md-9 bg-white">
                        <div className="row pt-3 border rounded">
                            <div className="col">
                                <h1>Users</h1>
                            </div>
                            <div className="col">
                                <ul className="pagination">
                                    {(this.state.pagesCount > 1) ? this.paginate() : ''}
                                </ul>
                            </div>
                            <div className="col-auto">
                                <form className="form-inline border rounded">
                                    <div className="form-group">
                                        <label htmlFor="search" className="sr-only">Search user</label>
                                        <input
                                            type="text"
                                            className="form-control border-0 rounded-left"
                                            id="search"
                                            placeholder="Search user"
                                            onChange={this.handleSearchUser}
                                        />
                                    </div>
                                    <select
                                        className="custom-select border-0 rounded-right" id="inputGroupSelect03"
                                        onChange={this.handleSearchBy.bind(this)}
                                    >
                                        <option value="name">by name</option>
                                        <option value="project">by project</option>
                                    </select>
                                </form>
                            </div>
                            <div className="col-auto">
                                <Link to={`/user/create`}>
                                        <span className="btn btn-outline-info">
                                            <i className="fas fa-plus"></i>
                                        </span>
                                </Link>
                            </div>
                        </div>
                        <table className="table table-striped">
                            <tbody>
                            {this.renderUsers()}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }

}

export default UserList
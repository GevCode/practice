import React, { Component } from 'react';
import {Link} from "react-router-dom";

export default class ProjectItemDiv extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const project = this.props.model;
        return(
            <div className="col-md-4 px-0">
                <Link to={`/project/${project.id}`}>
                    <div className="border bg-light text-center py-3">
                        <h5 className="text-dark">{project.title}</h5>
                        <p className="text-secondary">tasks: { project.tasks.length }</p>
                    </div>
                </Link>
            </div>
        )
    }
}
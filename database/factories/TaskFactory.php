<?php

use App\Task;
use Faker\Generator as Faker;

$factory->define(Task::class, function (Faker $faker) {
    return [
        'description' =>$faker->realText($maxNbChars = 200, $indexSize = 2)
    ];
});

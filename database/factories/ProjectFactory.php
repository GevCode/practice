<?php
use App\Project;
use Faker\Generator as Faker;

$factory->define(Project::class, function (Faker $faker) {
    return [
        'title' => $faker->realText($maxNbChars = 15, $indexSize = 2),
        'description' =>$faker->realText($maxNbChars = 200, $indexSize = 2)
    ];
});

<?php
use App\User;
use App\Project;
use App\Category;
use Illuminate\Database\Seeder;

class ProjectsTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        foreach(User::get() as $userObj) {
            if(rand(0,9)) {
                $projectCol = Factory(Project::class, rand(5,20))->make();
                $userObj->projects()->saveMany($projectCol);
            }
        }
    }
}

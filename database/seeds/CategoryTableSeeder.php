<?php

use Illuminate\Database\Seeder;
use App\Category;
use App\Project;

class CategoryTableSeeder extends Seeder
{
    protected $tags = [
        'tech',
        'finance',
        'marketing',
        'UI/UX',
        'web',
        'webinar',
        'web design',
        'web development',
        'interior design'
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tagIds = array_map(function ($tag) {
            return Category::Create(['category' => $tag])->id;
        }, $this->tags);
        foreach (Project::get() as $project) {
            $project
                ->categories()
                ->attach(array_map(function($key) use($tagIds) {
                    return $tagIds[$key];
                }, array_rand($tagIds,3)));
        }
    }
}

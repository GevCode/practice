<?php
use App\Project;
use App\Task;
use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        foreach(Project::get() as $projectObj) {
            if(rand(0,9)) {
                $taskCol = Factory(Task::class, rand(10, 50))->make();
                $projectObj->tasks()->saveMany($taskCol);
            }
        }
    }
}
